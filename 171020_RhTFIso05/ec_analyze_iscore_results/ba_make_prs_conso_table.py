# make a prs/rrs conso table
# will list genea, geneb, prs/rrs category, and score

path_prs_annot = '/Users/gms49/Documents/research_drive/files_ccsb/project_tf_isoforms/y2h/170323_prsv2.0/GDEhPRSv1.1_72prs_77rrs.tsv'
path_iscores = 'a_RhTFIso05_iscore_result_conso.tsv'

# read in prs annot data
d = {} # prs/rrs info, only v1.1 loaded
# plt_number -> well -> {'cat':PRS, 'A':ORCL2, 'B':MCM10}
for line in open(path_prs_annot):
    f = line.strip().split('\t') # f = fds
    src, gene_a, gene_b, cat, plt, well = f[0], f[3], f[6], f[7], f[10], f[11]
    if src != 'yes':
        continue
    if plt not in d:
        d[plt] = {}
    d[plt][well] = {}
    d[plt][well]['cat'] = cat
    d[plt][well]['A'] = gene_a
    d[plt][well]['B'] = gene_b

# read in prs iscore results for RhTFIso05
r = {} # (AD-centric) plt -> A or B -> well -> [3AT, CHX, Final]
# e.g. 1 -> A -> A04 -> [4, 0, 1]
for line in open(path_iscores):
    fds = line.split('\t')
    cat, plt_acc, well, score = fds[0:4]
    if 'AD' in plt_acc:
        plt = plt_acc[2] # 1 or 2 (AD-centric)
        grp = plt_acc[3] # A or B (AD-centric)
        try:
            score = int(score)
        except:
            score = 'NA'
        if plt not in r:
            r[plt] = {}
        if well not in r[plt]:
            r[plt][well] = {}
        if grp not in r[plt][well]:
            r[plt][well][grp] = ['', '']
        if cat == '3AT':
            r[plt][well][grp][0] = score
        elif cat == 'CHX':
            r[plt][well][grp][1] = score
        else:
            print 'error ' + line
# populate final scores based on score of pair on 3AT and CHX
for plt in r:
    for well in r[plt]:
        for grp, scores in r[plt][well].items():
            AT = scores[0]
            CX = scores[1]
            if 'NA' in scores:
                final_score = 'NA'
            elif AT >= CX + 2:
                final_score = 1
            elif AT == 1 and CX == 0:
                final_score = 1
            else:
                final_score = 0
            r[plt][well][grp].append(final_score)

# combine prs annot + score result, write-out annot table
ofile = open('ba_RhTFIso05_PRS_conso_table.tsv', 'w')
ofile2 = open('ba_RhTFIso05_PRS_not_in_v1.1.tsv', 'w')
ofile2.write('plate\twell\tad_grp\t3AT\tCHX\tfinal_score\n')
ofile.write('gene_a\tvector_a\tgene_b\tvector_b\tplate\twell\t3AT\tCHX\tfinal_score\tcat\n')
inv_grp = {'A':'B', 'B':'A'}
for plt in r:
    for well in r[plt]:
        for grp, scores in r[plt][well].items():
            ad_grp = grp
            db_grp = inv_grp[grp]
            AT, CX, FN = map(str, scores)
            # grab prs annot info
            try:
                cat = d[plt][well]['cat']
                ad_gene = d[plt][well][ad_grp]
                db_gene = d[plt][well][db_grp]
                gene_a, vect_a, gene_b, vect_b = ['', '', '', '']
                if ad_grp == 'A':
                    gene_a = ad_gene
                    vect_a = 'AD'
                    gene_b = db_gene
                    vect_b = 'DB'
                elif ad_grp == 'B':
                    gene_a = db_gene
                    vect_a = 'DB'
                    gene_b = ad_gene
                    vect_b = 'AD'
                else:
                    print 'error' + plt, well, grp, scores
                odata = [gene_a, vect_a, gene_b, vect_b, plt, well, AT, CX, FN, cat]
                ofile.write('\t'.join(odata) + '\n')
            except:
                AT, CX, FN = map(str, scores)
                odata = [plt, well, grp, AT, CX, FN]
                ofile2.write('\t'.join(odata) + '\n')
