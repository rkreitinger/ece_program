# 171102
# finished manually scoring R5 plates (3AT, CHX, SCLW)
# combine scores into one table


import glob
import os
import subprocess

ofile = open('a_RhTFIso05_iscore_result_conso.tsv', 'w')
ofile.write('media\tretest_plt\tretest_well\tiscore\tcolony_intensity\tscore_mode\n')
for file in glob.glob('../eb_iscore_results/RhTFIso05r01*/*tsv'):
    fname = os.path.basename(file)
    prefix, cat, plt = fname.split('.')[0].split('_')
    if plt == 'xx':
        continue
    subprocess.call('mac2unix {}'.format(file), shell=True)
    for line in open(file).readlines()[0:96]:
        fds = line.strip().split('\t')
        if len(fds) > 1:
            odata = [cat, plt] + fds
            ofile.write('\t'.join(odata) + '\n')
