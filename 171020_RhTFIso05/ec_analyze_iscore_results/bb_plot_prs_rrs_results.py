# plot the prs/rrs recovery for R5

import matplotlib.pyplot as plt
import numpy as np

# read in prs/rrs data
d = {'PRS':{}, 'RRS':{}} # prs -> pair -> score
for line in open('ba_RhTFIso05_PRS_conso_table.tsv').readlines()[1:]:
    fds = line.rstrip().split('\t')
    gene_a, vect_a, gene_b, vect_b, plate, well, at, cx, fn, cat = fds
    fn = int(fn) # final score
    pair = (gene_a, gene_b)
    # if AD is gene_a, then score in index 0
    # if DB is gene_a, then score in index 1
    if pair not in d[cat]:
        d[cat][pair] = ['', '', ''] # [score_config1, score_config2, conso_score]
    if vect_a == 'AD':
        d[cat][pair][0] = fn
    else:
        d[cat][pair][1] = fn
for cat in d:
    for pair, scores in d[cat].items():
        s1, s2, f = scores
        if s1 == 1 or s2 == 1:
            f = 1
        else:
            f = 0
        d[cat][pair][2] = f


tot_prs = len(d['PRS'])
tot_rrs = len(d['RRS'])

pos_rrs = 0
for pair, scores in d['RRS'].items():
    fin_score = scores[-1]
    if fin_score == 1:
        pos_rrs += 1

pos_prs = 0
for pair, scores in d['PRS'].items():
    fin_score = scores[-1]
    if fin_score == 1:
        pos_prs += 1

# recoveries of prs/rrs
rec_rrs = pos_rrs/float(tot_rrs)
rec_prs = pos_prs/float(tot_prs)

ind = np.arange(2)
width = 0.35
fig, ax = plt.subplots(figsize=(4,3))
barlist = ax.bar(ind + width, [rec_prs, rec_rrs], color='b', edgecolor='black')
plt.grid(linestyle='dashed')
ax.set_axisbelow(True)
barlist[1].set_color('r')
ax.set_xticks(ind + width)
ax.set_xticklabels(('PRS', 'RRS'))
plt.title('PRS v1.1 recoveries (75 RRS, 72 PRS)')
plt.ylabel('% recovery')
plt.ylim(0, 1)

plt.savefig('bb_PRS_recovery_plot.pdf')
