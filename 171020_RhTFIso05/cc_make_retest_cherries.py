# from cherry table, make gwl file to cp retest plates
# considerations - each group of 18 plates cherry picked with each other
# considreations - 4KDB, 4KAD, and HuRIAD V2 in their separate runs

import os

data = []
with open('cc_retest_cherry_table.tsv', 'w') as ofile:
    ofile.write('cherry_grp\tad_grp\tdb_grp\tad__orfid\tdb_orfid\tad_plt\tad_pos\tdb_plt\tdb_pos\tretest_pla\tretest_pos\n')
    for line in open('ab_retest_cherry_table.tsv').readlines()[1:]:
        fds = line.rstrip().split('\t')
        ad, db, adplt, adpos, dbplt, dbpos, rplt, rpos = fds
        if int(rplt) <= 18:
            grp = 1
        if 18 < int(rplt) <= 36:
            grp = 2
        if 36 < int(rplt):
            grp = 3
        if '_2' in adplt:
            ad_cat = 'huri'
        elif 'Iso4' in adplt:
            ad_cat = '4kad'
        elif 'Hub' in adplt:
            ad_cat = '4kad'
        else:
            print 'error:' + adplt
        db_cat = '4kdb'
        odata = map(str, [grp, ad_cat, db_cat, ad, db, adplt, adpos, dbplt, dbpos, rplt, rpos])
        data.append(odata)
        ofile.write('\t'.join(odata) + '\n')

d = {}
# grp -> ad_cat -> ad_plt/pos -> rt_plt/pos
# grp -> db_cat -> db_plt/pos -> rt_plt/pos
for row in data:
    grp, ad_cat, db_cat, ad, db, adplt, adpos, dbplt, dbpos, rplt, rpos = row
    if grp not in d:
        d[grp] = {}
    if ad_cat not in d[grp]:
        d[grp][ad_cat] = {}
    ad_info = adplt, adpos
    if ad_info not in d[grp][ad_cat]:
        d[grp][ad_cat][ad_info] = []
    d[grp][ad_cat][ad_info].append((rplt, rpos))
    if db_cat not in d[grp]:
        d[grp][db_cat] = {}
    db_info = dbplt, dbpos
    if db_info not in d[grp][db_cat]:
        d[grp][db_cat][db_info] = []
    d[grp][db_cat][db_info].append((rplt, rpos))

# write out cherry files
cp_dir = './cc_retest_cherry_files/'
if not os.path.exists(cp_dir):
    os.mkdir(cp_dir)
i = 0 # number of 'aspirates' to calc. num. 200ul tips needed
for grp in sorted(d):
    for cat in sorted(d[grp]):
        fname = 'cherry_grp_{}_categ_{}.gwl'.format(grp, cat)
        with open(cp_dir + fname, 'w') as ofile:
            for pos_info in sorted(d[grp][cat]):
                splt, spos = pos_info
                vol = len(d[grp][cat][pos_info]) * 5
                ofile.write('A;;{};HitPick96Costar;{};;{}'.format(splt, spos, vol) + '\r\n')
                i += 1
                for dplt, dpos in d[grp][cat][pos_info]:
                    if 'db' in cat:
                        dplt_name = 'RhTFIso05r01_{:0>3}DB'.format(int(dplt))
                    else:
                        dplt_name = 'RhTFIso05r01_{:0>3}AD'.format(int(dplt))
                    ofile.write('D;;{};HitPick96Costar;{};;5\r\n'.format(dplt_name, dpos))
                ofile.write('W;\r\n')
print 'num of 200ul tips used:' + str(i)


# QC - check that each well not hit at more than 30 times
d2 = {}  # plt/pos info -> list of src positions
for row in data:
    grp, ad_cat, db_cat, ad, db, adplt, adpos, dbplt, dbpos, rplt, rpos = row
    adinfo = adplt, adpos
    dbinfo = dbplt, dbpos
    if adinfo not in d2:
        d2[adinfo] = []
    if dbinfo not in d2:
        d2[dbinfo] = []
    d2[adinfo].append((adplt, adpos))
    d2[dbinfo].append((dbplt, dbpos))
for k, v in d2.items():
    l = len(v)
    if l > 30:
        print l
# result - all plate/pos only hit at up to 30 times, QC check good
