#retest plate image to pair-specific spots
# each tile saved as sep. jpg with <plate_num>_<well>_<media>.jpg

from PIL import Image
import cv2 as cv
import math
import os
import glob

pos_array = ['A01', 'B01', 'C01', 'D01', 'E01', 'F01', 'G01', 'H01', 'A02', 'B02', 'C02', 'D02',
             'E02', 'F02', 'G02', 'H02', 'A03', 'B03', 'C03', 'D03', 'E03', 'F03', 'G03', 'H03',
             'A04', 'B04', 'C04', 'D04', 'E04', 'F04', 'G04', 'H04', 'A05', 'B05', 'C05', 'D05',
             'E05', 'F05', 'G05', 'H05', 'A06', 'B06', 'C06', 'D06', 'E06', 'F06', 'G06', 'H06',
             'A07', 'B07', 'C07', 'D07', 'E07', 'F07', 'G07', 'H07', 'A08', 'B08', 'C08', 'D08',
             'E08', 'F08', 'G08', 'H08', 'A09', 'B09', 'C09', 'D09', 'E09', 'F09', 'G09', 'H09',
             'A10', 'B10', 'C10', 'D10', 'E10', 'F10', 'G10', 'H10', 'A11', 'B11', 'C11',
             'D11', 'E11', 'F11', 'G11', 'H11', 'A12', 'B12', 'C12', 'D12', 'E12', 'F12', 'G12', 'H12']


for file in glob.glob('../eb_iscore_results/Rh*/ForColonyzer/*jpg'):
   
    print (file)
    fname = os.path.basename(file)
   
    print (fname)
   
    pla_name = fname.split(".")[0]
    print (pla_name)
    img = Image.open(file)
    w, h = img.size
    x_unit = w /12
    y_unit = h / 8

    

    img_dir = file.split("/")[2]
    if not os.path.exists(img_dir):
        os.mkdir(img_dir)

    for i in range(0, 12):
        for j in range(0, 8):
                x = x_unit * i
                y = y_unit * j
                pos = (i % 12) * 8 + (j % 8)
                pla_pos = math.floor(i / 12) * 3 + math.floor(j / 8)
                well = pos_array[int(pos)]
                tile = img.crop((x, y, x + x_unit, y + y_unit))
                tile.save(img_dir + '/{}_{}.jpg'.format(pla_name, well))
                image = cv.imread(img_dir + '/{}_{}.jpg'.format(pla_name, well))
                gray_image = cv.cvtColor(image, cv2.COLOR_BGR2GRAY)
                cv.imwrite(img_dir + '/gray_{}_{}.jpg'.format(pla_name, well))
                cv.imshow('hi', image)
                cv.imshow('bye', gray_image)
