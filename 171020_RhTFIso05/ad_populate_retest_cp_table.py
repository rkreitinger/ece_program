# transfer data from retest to retest_cp table

import MySQLdb
import pickle

con = MySQLdb.connect('paros.dfci.harvard.edu', 'gloria', 'abc123', 'tf_isoforms')
cur = con.cursor()

ad_pos = pickle.load(open('./c_gene_orf_pos_map_pickles/c_ad_pos.p'))
db_pos = pickle.load(open('./c_gene_orf_pos_map_pickles/c_db_pos.p'))

cmd = 'select retest_id, ad_orfid, db_orfid, retest_batch from retest where standard_batch="RhTFIso05"'
cur.execute(cmd)
rows = cur.fetchall()
for row in rows:
    retest_id, ad_orf, db_orf, retest_batch = row
    try:
        ad_plate, ad_well = ad_pos[ad_orf]
        db_plate, db_well = db_pos[db_orf]
        cmd = ('insert into retest_cp values '
               '({}, {}, {}, NULL, NULL, "{}", '
               '"RhTFIso05", 0, 0, "{}", "{}", "{}", "{}")'.format(retest_id, ad_orf, db_orf, retest_batch, ad_plate, ad_well, db_plate, db_well))
        cur.execute(cmd)
    except:
        print row
        # one interaction between KCNJ18 and RORC -> no ad was ever picked, skip these
