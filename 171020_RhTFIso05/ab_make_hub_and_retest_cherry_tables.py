# 171021
# make cherry retest plate info
# 1603 pairs to test in RhTFIso05
# three replicates, each randomized within 18 plates (1656 well positions)
# previously, I did this using Tong' scripts, but too complex, so doing all here

import pickle
import math
import random
import copy


# load ad_orf and db_orf position data
ad_well_dict = pickle.load(open('./c_gene_orf_pos_map_pickles/c_ad_pos.p'))
db_well_dict = pickle.load(open('./c_gene_orf_pos_map_pickles/c_db_pos.p'))

#    plate map
posArray=["A01","B01","C01","D01","E01","F01","G01","H01","A02","B02","C02","D02",
          "E02","F02","G02","H02","A03","B03","C03","D03","E03","F03","G03","H03",
          "A04","B04","C04","D04","E04","F04","G04","H04","A05","B05","C05","D05",
          "E05","F05","G05","H05","A06","B06","C06","D06","E06","F06","G06","H06",
          "A07","B07","C07","D07","E07","F07","G07","H07","A08","B08","C08","D08",
          "E08","F08","G08","H08","A09","B09","C09","D09","E09","F09","G09","H09",
          "A10","B10","C10","D10","E10","F10","G10","H10","A11","B11","C11",
          "D11","E11","F11","G11","H11","A12","B12","C12","D12","E12","F12","G12","H12"];
wellMap = {}
for i, well in enumerate(posArray):
    wellMap[well] = i + 1

# convert well to pos
ad_pos_dict = {}
for orf, [pla, well] in ad_well_dict.items():
    pos = wellMap[well]
    ad_pos_dict[orf] = [pla, pos]
db_pos_dict = {}
for orf, [pla, well] in db_well_dict.items():
    pos = wellMap[well]
    db_pos_dict[orf] = [pla, pos]
print ad_pos_dict

# %%



# load pairs data
pairs = []
for line in open('aa_retest_source_data.tsv').readlines()[1:]:
    fds = line.strip().split('\t')
    ad_orf, db_orf, ad, db, src, rbatch, sbatch = fds
    pairs.append([int(ad_orf), int(db_orf), rbatch])

# assign hub plates

# load counts of ad/db orfs needed for retest
ad_cts = {} # ad_orfid -> ct
db_cts = {} # db_orfid -> ct
for pair in pairs:
    ad_orf, db_orf = pair[0:2]
    if ad_orf not in ad_cts:
        ad_cts[ad_orf] = 0
    ad_cts[ad_orf] += 1
    if db_orf not in db_cts:
        db_cts[db_orf] = 0
    db_cts[db_orf] += 1

# assing hub plate/pos info, for those hubs (>30)
ad_plt = 'RhTFIso05_HubAD01'
db_plt = 'RhTFIso05_HubDB01'
ad_hub = {} # ad_orfid -> [[src_plt, src_pos, ct], [src_plt, src_pos, ct]]
db_hub = {} # same as ad_orfid
cur_pos = 0
with open('ab_ad_hub_pos.tsv', 'w') as ofile, open('ab_db_hub_pos.tsv', 'w') as ofile2:
    ofile.write('ad_orfid\tad_src_plt\tad_src_pos\tad_hub_plt\tad_hub_pos\n')
    ofile2.write('db_orfid\tdb_src_plt\tdb_src_pos\tdb_hub_plt\tdb_hub_pos\n')
    for orfid, ct in ad_cts.items():
        ct = ct * 3
        if ct > 30:
            num_pos = int(math.ceil(ct/30.0))
            for i in range(num_pos):
                cur_pos += 1
                if orfid not in ad_hub:
                    ad_hub[orfid] = []
                ad_hub[orfid].append([ad_plt, cur_pos, 30])
                ad_src_plt, ad_src_pos = ad_pos_dict[orfid]
                ofile.write('\t'.join(map(str,[orfid, ad_src_plt, ad_src_pos, ad_plt, cur_pos])) + '\n')
    cur_pos = 0
    for orfid, ct in db_cts.items():
        ct = ct * 3
        if ct > 30:
            num_pos = int(math.ceil(ct/30.0))
            for i in range(num_pos):
                cur_pos += 1
                if cur_pos > 94:
                    db_plt = 'RhTFIso05_HubDB02'
                    cur_pos = 1
                if orfid not in db_hub:
                    db_hub[orfid] = []
                db_hub[orfid].append([db_plt, cur_pos, 30])
                db_src_plt, db_src_pos = db_pos_dict[orfid]
                ofile2.write('\t'.join(map(str,[orfid, db_src_plt, db_src_pos, db_plt, cur_pos])) + '\n')

# %%

# ~18 plates for all pairs to test in R5
# will have 3 groups of 18 plates or 54 plates total
# pairs within each group will be shuffled

random.seed(1)

def plate_barcode (plate_id, pos_num):
    # tong's function to make secrete code
    empty = {}
    if pos_num == 2:
        if (plate_id < 256) :
            pos1 = plate_id/16 + 1
            pos2 =  plate_id % 16 + 17
        else:
            pos1 = (plate_id-256)/16+ 49
            pos2 = ( plate_id-256) % 16 + 65
        empty[pos1] = 1
        empty[pos2] = 1
    if pos_num == 3:
        pos1 = plate_id/64 + 1
        pos2 = plate_id % 64 / 8 + 9
        pos3 = plate_id % 8 + 17
        empty[pos1] = 1
        empty[pos2] = 1
        empty[pos3] = 1
    return empty

print plate_barcode(0, 2)
print plate_barcode(2, 2)
print plate_barcode(3, 2)


ofile = open('ab_retest_cherry_table.tsv', 'w')
ofile.write('ad_orf\tdb_orf\tad_src_plt\tad_src_pos\tdb_src_plt\tdb_src_pos\tretest_pla\tretest_pos\n')

pairs_grp2 = copy.deepcopy(pairs)
random.shuffle(pairs_grp2)

pairs_grp3 = copy.deepcopy(pairs)
random.shuffle(pairs_grp3)

for retest_pla, pairs in ((1, pairs), (19, pairs_grp2), (37, pairs_grp3)):
    # first group not shuffled, group 2 and 3 shuffled
    retest_pos = 0
    for pair in pairs:
        retest_pos += 1
        if retest_pos > 94:
            retest_pla += 1
            retest_pos = 1
        # secret code
        if retest_pos in plate_barcode(retest_pla, 2):
            retest_pos += 1
        ad_orf, db_orf, rbatch = pair
        ad_src_plt, ad_src_plt = 'xxx', 'xxx'
        if ad_orf in ad_hub:
            pos_infos = ad_hub[ad_orf]
            for i, pos_info in enumerate(pos_infos):
                ad_plt, ad_pos, ct = pos_info
                if ct < 1:
                    continue
                ad_src_plt, ad_src_pos = ad_plt, ad_pos
                pos_infos[i][-1] -= 1 # decrement to track  well usage
                break
        else:
            ad_src_plt, ad_src_pos = ad_pos_dict[ad_orf]
        db_src_plt, db_src_plt = 'xxx', 'xxx'
        if db_orf in db_hub:
            pos_infos = db_hub[db_orf]
            for i, pos_info in enumerate(pos_infos):
                db_plt, db_pos, ct = pos_info
                if ct < 1:
                    continue
                db_src_plt, db_src_pos = db_plt, db_pos
                pos_infos[i][-1] -= 1 # decrement to track  well usage
                break
        else:
            db_src_plt, db_src_pos = db_pos_dict[db_orf]
        odata = [ad_orf, db_orf, ad_src_plt, ad_src_pos, db_src_plt, db_src_pos, retest_pla, retest_pos]
        ofile.write('\t'.join(map(str,odata)) + '\n')
