# put plate images into dirs, modify names so media-type is with prefix

import glob
import os

for file in glob.glob('RhTFIso*'):
    new_file = ''
    for cat in ['CHX', 'SCLW']:
        if cat in file:
            new_file = file.replace('_'+cat, '').replace('r01','r01_'+cat)
    if not new_file:
        cat = '3AT'
        new_file = file.replace('_'+cat, '').replace('r01','r01_'+cat)
    new_dir = new_file.split('.')[0]
    os.mkdir(new_dir)
    os.rename(file, new_dir + '/' + new_file)
