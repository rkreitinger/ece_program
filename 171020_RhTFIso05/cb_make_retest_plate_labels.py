# make labels of retest plates

import sys
sys.stdout = open('cb_RhTFIso05_retest_plate_names.txt', 'w')

for i in range(1, 55):
    print 'RhTFIso05r01_{:0>3}AD'.format(i)

for i in range(1, 55):
    print 'RhTFIso05r01_{:0>3}DB'.format(i)
