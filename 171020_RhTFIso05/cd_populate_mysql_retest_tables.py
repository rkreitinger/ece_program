# B/c of complexity involved, I wrote python scripts to define hub and
# retest plates.
# Therefore, I am retrospectively uploading data to mysql tables.

import MySQLdb

con = MySQLdb.connect('paros.dfci.harvard.edu', 'gloria', 'abc123', 'tf_isoforms')
cur = con.cursor()

# need genenames for orfid
orfs = {} # orfid -> genename
for line in open('aa_retest_source_data.tsv').readlines()[1:]:
    fds = line.strip().split('\t')
    adorf, dborf, ad, db, src, rbatch, sbatch = fds
    if adorf in orfs:
        print 'dup orfid:' + str(adorf)
    if dborf in orfs:
        print 'dup orfid:' + str(dborf)
    orfs[adorf] = ad
    orfs[dborf] = db


#	plate map
posArray=["A01","B01","C01","D01","E01","F01","G01","H01","A02","B02","C02","D02",
		  "E02","F02","G02","H02","A03","B03","C03","D03","E03","F03","G03","H03",
		  "A04","B04","C04","D04","E04","F04","G04","H04","A05","B05","C05","D05",
		  "E05","F05","G05","H05","A06","B06","C06","D06","E06","F06","G06","H06",
		  "A07","B07","C07","D07","E07","F07","G07","H07","A08","B08","C08","D08",
		  "E08","F08","G08","H08","A09","B09","C09","D09","E09","F09","G09","H09",
		  "A10","B10","C10","D10","E10","F10","G10","H10","A11","B11","C11",
		  "D11","E11","F11","G11","H11","A12","B12","C12","D12","E12","F12","G12","H12"];
wellMap = {}
for i, well in enumerate(posArray):
	wellMap[well] = i + 1

retest_id = 18000
for line in open('cc_retest_cherry_table.tsv').readlines()[1:]:
    fds = line.rstrip().split('\t')
    grp, adgrp, dbgrp, adorf, dborf, adplt, adpos, dbplt, dbpos, rplt, rpos = fds
    rwell = posArray[int(rpos)-1]
    dwell = posArray[int(dbpos)-1]
    awell = posArray[int(adpos)-1]
    ad = orfs[adorf]
    db = orfs[dborf]
    if 'Iso4' in adplt:
        cat = 'RhTFIso05B'
    else:
        cat = 'RhTFIso05A'
    query = 'insert into retest values ({}, "{}", "{}", {}, {}, {}, "{}", "{}", "RhTFIso05", {}, {}, {}, "{}")'.format(retest_id, ad, db, adorf, dborf, rplt, rwell, cat, 'NULL', 'NULL', 'NULL', db + '_' + ad)
    cur.execute(query)
    query = 'insert into retest_cp values ({}, {}, {}, {}, "{}", "{}", "RhTFIso05", 0, 0, "{}", "{}", "{}", "{}")'.format(retest_id, adorf, dborf, rplt, rwell, cat, adplt, awell, dbplt, dwell)
    cur.execute(query)
    retest_id += 1
