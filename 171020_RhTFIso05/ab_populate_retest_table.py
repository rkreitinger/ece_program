# populate the 'retest' table

import MySQLdb

con = MySQLdb.connect('paros.dfci.harvard.edu', 'gloria', 'abc123', 'tf_isoforms')
cur = con.cursor()

retest_id = 18000

# ensure all RhTFIso05 entries are deleted
cmd = 'delete from retest where standard_batch="RhTFIso05"'
cur.execute(cmd)

cmd = 'select * from retest_source where standard_batch="RhTFIso05"'
cur.execute(cmd)
rows = cur.fetchall()
for row in rows:
    ad_orf, db_orf, ad_symbol, db_symbol, src, retest_batch, std_batch = row
    if 'unmat' in src:
        retest_batch='RhTFIso05A'
    elif 'bimat' in src:
        retest_batch='RhTFIso05B'
    # checked and all src fulfills conditions above
    cmd = ('insert into retest values '
           '({}, "{}", "{}", {}, {}, NULL, NULL, '
           '"{}", "RhTFIso05", '
           'NULL, NULL, NULL, "{}_{}")'.format(retest_id, ad_symbol, db_symbol, ad_orf, db_orf, retest_batch, db_symbol, ad_symbol))
    cur.execute(cmd)
    retest_id += 1
    cmd = ('insert into retest values '
           '({}, "{}", "{}", {}, {}, NULL, NULL, '
           '"{}", "RhTFIso05", '
           'NULL, NULL, NULL, "{}_{}")'.format(retest_id, ad_symbol, db_symbol, ad_orf, db_orf, retest_batch, db_symbol, ad_symbol))
    cur.execute(cmd)
    retest_id += 1
    cmd = ('insert into retest values '
           '({}, "{}", "{}", {}, {}, NULL, NULL, '
           '"{}", "RhTFIso05", '
           'NULL, NULL, NULL, "{}_{}")'.format(retest_id, ad_symbol, db_symbol, ad_orf, db_orf, retest_batch, db_symbol, ad_symbol))
    cur.execute(cmd)
    retest_id += 1
