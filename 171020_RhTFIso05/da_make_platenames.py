# make platenames for mating, select, spotting

import sys
orig_stdout = sys.stdout
sys.stdout = open('da_RhTFIso05_platenames.txt', 'w')

for cat in 'mate YPD', 'select SC-LW':
    for i in range(1, 55):
        print 'RhTFIso05r01_{:0>2} {}'.format(i, cat)
    print 'RhTFIso05_PRS_AD1A_DB1B {}'.format(cat)
    print 'RhTFIso05_PRS_AD1B_DB1A {}'.format(cat)
    print 'RhTFIso05_PRS_AD2A_DB2B {}'.format(cat)
    print 'RhTFIso05_PRS_AD2B_DB2A {}'.format(cat)
    print 'RhTFIso05_6ctrl {}'.format(cat)

# sys.stdout = orig_stdout

# make spot platenames
for i in range(1, 54, 6):
    print 'RhTFIso05r01_{:0>2}_{:0>2}_{:0>2}_{:0>2}_{:0>2}_{:0>2}'.format(i,i+1,i+2,i+3,i+4,i+5)
for i in range(1, 54, 6):
    print 'RhTFIso05r01_{:0>2}_{:0>2}_{:0>2}_{:0>2}_{:0>2}_{:0>2}_CHX'.format(i,i+1,i+2,i+3,i+4,i+5)
for i in range(1, 54, 6):
    print 'RhTFIso05r01_{:0>2}_{:0>2}_{:0>2}_{:0>2}_{:0>2}_{:0>2}_SCLW'.format(i,i+1,i+2,i+3,i+4,i+5)
print 'RhTFISo05r01_AD1A_AD1B_AD2A_AD2B_xx_xx'
print 'RhTFISo05r01_AD1A_AD1B_AD2A_AD2B_xx_xx_CHX'

# make lyse platenames (after spotting, will lyse plates)
for i in range(1, 55):
    print 'lyse_RhTFIso05r01_{:0>2} GS171028'.format(i)
