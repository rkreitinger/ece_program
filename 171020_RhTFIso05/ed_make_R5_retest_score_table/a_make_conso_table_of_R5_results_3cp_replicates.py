# make a conso table of all retest results
# 'a_R5_iscore_conso.tsv' - list iscore results for each pair
# 'a_R5_iscore_conso_3reps_merged.tsv' - one row for each pair, 3 tech reps listed in that row

import pickle

path_retest_cp = '../cc_retest_cherry_table.tsv'
path_conso_iscores = '../ec_analyze_iscore_results/a_RhTFIso05_iscore_result_conso.tsv'

ofile1 = open('a_R5_iscore_conso.tsv', 'w')
ofile2 = open('a_R5_iscore_conso_3reps_merged.tsv', 'w')

# make an orf->name lookup
db_orfs = pickle.load(open('../c_gene_orf_pos_map_pickles/c_db_orfs.p'))
ad_orfs = pickle.load(open('../c_gene_orf_pos_map_pickles/c_ad_orfs.p'))
orf_name = {} # orfid -> genename
for d in db_orfs, ad_orfs:
    for name, orfs in d.items():
        for orf in orfs:
            orf_name[str(orf)] = name

#    plate map
posArray=["A01","B01","C01","D01","E01","F01","G01","H01","A02","B02","C02","D02",
          "E02","F02","G02","H02","A03","B03","C03","D03","E03","F03","G03","H03",
          "A04","B04","C04","D04","E04","F04","G04","H04","A05","B05","C05","D05",
          "E05","F05","G05","H05","A06","B06","C06","D06","E06","F06","G06","H06",
          "A07","B07","C07","D07","E07","F07","G07","H07","A08","B08","C08","D08",
          "E08","F08","G08","H08","A09","B09","C09","D09","E09","F09","G09","H09",
          "A10","B10","C10","D10","E10","F10","G10","H10","A11","B11","C11",
          "D11","E11","F11","G11","H11","A12","B12","C12","D12","E12","F12","G12","H12"];
wellMap = {}
for i, well in enumerate(posArray):
    wellMap[well] = i + 1

# read-in retest pairs info
d = {} # (retest_plt, retest_pos) -> retest cp info
# 3 sub-dict of 3 groups
d2 = {} # (ad_orf, db_orf) -> grp -> info
for line in open(path_retest_cp).readlines()[1:]:
    fds = line.rstrip().split('\t')
    grp, ad_src, db_src, ad_orf, db_orf = fds[0:5]
    if ad_src == 'huri':
        cat = 'unmat'
    else:
        cat = 'bimat'
    rt_plt, rt_pos = fds[9], fds[10]
    rt_well = posArray[int(rt_pos)-1]
    ad_gene = orf_name[ad_orf]
    db_gene = orf_name[db_orf]
    acc = (rt_plt, rt_well)
    if acc in d:
        print 'error duplicate ' + line
    d[acc] = [cat, ad_src, ad_gene, ad_orf, db_src, db_gene, db_orf, grp, rt_plt, rt_well]
    acc2 = (ad_orf, db_orf)
    if acc2 not in d2:
        d2[acc2] = {}
    d2[acc2][grp] = [cat, ad_src, ad_gene, db_src, db_gene, rt_plt, rt_well]
print d2

# read in retest score info
s = {} # s=score info, (retest_plt, retest_pos) -> [AT_score, CX_score, LW_score, FN_score]
for line in open(path_conso_iscores).readlines()[1:]:
    fds = line.rstrip().split('\t')
    media, rt_plt, rt_well, score = fds[0:4]
    try:
        rt_plt = str(int(rt_plt))
    except:
        pass
    acc = (rt_plt, rt_well)
    if acc not in s:
        s[acc] = ['', '', '', '']
    # convert score to int, NAs stay as string
    try:
        score = int(score)
    except:
        score = 'NA'
    if media == '3AT':
        s[acc][0] = score
    elif media == 'CHX':
        s[acc][1] = score
    # lw scored in binary mode half the time
    # some lw plates have score of 4 for big fats
    # some lw plates have score of 1 for big fats
    elif media == 'SCLW':
        if score == 'NA':
            score = 'NA'
        elif int(score) >= 3 or int(score) == 1:
            score = '1'
        else:
            score = 'NA'
        s[acc][2] = score
    else:
        print 'error no media match: ' + line
# set final scores
uniq_scores = {}
for acc, scores in s.items():
    at, cx, lw = scores[0:3]
    if 'NA' in (at, cx, lw):
        fn = 'NA'
    else:
        # calculate final scores
        if at >= (cx + 2):
            fn = 1
        elif cx >= 2:
            fn = 'AA'
        else:
            fn = 0
    s[acc][3] = fn
    scoreset = (at, cx, lw, fn)
    if scoreset not in uniq_scores:
        uniq_scores[scoreset] = 0
    uniq_scores[scoreset] += 1

# write-out freq of score sets
with open('a_freq_of_score_patterns.tsv', 'w') as ofile:
    ofile.write('AT\tCX\tLW\tFN\tct\n')
    for scoreset in sorted(uniq_scores):
        ofile.write('\t'.join(map(str, scoreset)) + '\t' + str(uniq_scores[scoreset]) + '\n')

# write-out conso tables
ofile1.write('cat\tad_src\tad_gene\tad_orf\tdb_src\tdb_gene\tdb_orf\tgrp\trt_plt\trt_well\tat\tcx\tlw\tfn\n')
for acc in d:
    at, cx, lw, fn = s[acc]
    odata = d[acc] + s[acc]
    ofile1.write('\t'.join(map(str, odata)) + '\n')

# write-out conso table with group scores on same row
ofile2.write('cat\tad_src\tad_gene\tad_orf\tdb_src\tdb_gene\tdb_orf\tg1plt\tg1well\tg1_at\tg1_cx\tg1_lw\tg2plt\tg2well\tg2_at\tg2_cx\tg2_lw\tg3plt\tg3well\tg3_at\tg3_cx\tg3_lw\tnum_reps\tavg_at\tavg_cx\tavg_lw\tfin_score\n')
for (ad_orf, db_orf), val in d2.items():
    g1_plt = val['1'][-2]
    g1_well = val['1'][-1]
    g2_plt = val['2'][-2]
    g2_well = val['2'][-1]
    g3_plt = val['3'][-2]
    g3_well = val['3'][-1]
    cat, ad_src, ad_gene, db_src, db_gene = val['1'][0:5]
    num_vals = 0 # number of values averaged (skip over cases of 1+ NAs)
    # will get average score of at, cx, lw
    at = []
    cx = []
    lw = []
    g1_acc = (g1_plt, g1_well)
    g1_at, g1_cx, g1_lw, g1_fn = s[g1_acc]
    g2_acc = (g2_plt, g2_well)
    g2_at, g2_cx, g2_lw, g2_fn = s[g2_acc]
    g3_acc = (g3_plt, g3_well)
    g3_at, g3_cx, g3_lw, g3_fn = s[g3_acc]
    try:
        if 'NA' not in s[g1_acc]:
            at.append(int(g1_at))
            cx.append(int(g1_cx))
            lw.append(int(g1_lw))
        if 'NA' not in s[g2_acc]:
            at.append(int(g2_at))
            cx.append(int(g2_cx))
            lw.append(int(g2_lw))
        if 'NA' not in s[g3_acc]:
            at.append(int(g3_at))
            cx.append(int(g3_cx))
            lw.append(int(g3_lw))
    except:
        print s[g3_acc], g3_plt, g3_well
    num_reps = len(at) # number of tech reps used
    if len(at) > 0:
        avg_at = sum(at)/float(len(at))
    else:
        avg_at = 'NA'
    if len(cx) > 0:
        avg_cx = sum(cx)/float(len(cx))
    else:
        avg_cx = 'NA'
    if len(lw) > 0:
        avg_lw = sum(lw)/float(len(lw))
    else:
        avg_lw = 'NA'
    # determine final score
    if 'NA' in (avg_at, avg_cx, avg_lw):
        fin_sc = 'NA'
    else:
        if avg_cx == 0:
            if avg_at >= 1:
                fin_sc = '1'
            else:
                fin_sc = '0'
        elif avg_cx > 0:
            if avg_at >= (avg_cx + 1.5):
                fin_sc = '1'
            elif avg_at < 1 and avg_cx < 1:
                fin_sc = '0'
            elif (avg_at + 1.5) < avg_cx:
                fin_sc = 'NA'
            else:
                fin_sc = 'AA'
    odata = [cat, ad_src, ad_gene, ad_orf, db_src, db_gene, db_orf]
    odata.extend([g1_plt, g1_well, g1_at, g1_cx, g1_lw])
    odata.extend([g2_plt, g2_well, g2_at, g2_cx, g2_lw])
    odata.extend([g3_plt, g3_well, g3_at, g3_cx, g3_lw])
    odata.extend([num_reps, avg_at, avg_cx, avg_lw, fin_sc])
    ofile2.write('\t'.join(map(str, odata)) + '\n')
