# selected pairs part of iso-matrices from R4 data
# upload the pair data to 'retest_source' table

import MySQLdb

con = MySQLdb.connect('paros', 'gloria', 'abc123', 'tf_isoforms')
cur = con.cursor()

ofile = open('aa_retest_source_data.tsv', 'w')
ofile.write('ad_orfid\tdb_orfid\tad_symbol\tdb_symbol\tsource\tretest_batch\tstandard_batch\n')
for line in open('../171016_RhTFIso04r01/e_generate_final_pairs_to_test_for_R5/db_R4_retest_table_w_rm_rows_marked.tsv').readlines()[1:]:
    fds = line.strip().split('\t')
    if fds[-1] != 'in_iso_matrix':
        continue
    ad = fds[1]
    db = fds[2]
    adorf = fds[3]
    dborf = fds[4]
    batch_cat = fds[7]
    if batch_cat == 'RhTFIso04A':
        batch = 'RhTFIso05A'
        cat = 'unmat_matrix'
    elif batch_cat == 'RhTFIso04B':
        batch = 'RhTFIso05B'
        cat = 'bimat_matrix'
    query = 'insert into retest_source values ({}, {}, "{}", "{}", "{}", "{}", "RhTFIso05")'.format(adorf, dborf, ad, db, cat, batch)
    cur.execute(query)
    ofile.write('\t'.join([adorf, dborf, ad, db, cat, batch, 'RhTFIso05']) + '\n')
