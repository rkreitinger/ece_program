# for RhTFIso04, I already made pickles of ->
# ad_orfs = {} # gene -> list of orfids
# db_orfs = {} # gene -> list of orfids
# ad_pos = {} # orf -> (ad_plt, ad_well)
# db_pos = {} # orf -> (db_plt, db_well)

import subprocess
import glob
import os

if not os.path.exists('c_gene_orf_pos_map_pickles'):
    os.mkdir('c_gene_orf_pos_map_pickles')

dir = '/Volumes/mvidal/projects/TF_Iso_Var/Y2H_TF_Iso/171005_RhTFIso04/a_retest_plates_define_and_cp/c_gene_orf_pos_map_pickles'

for pfile in glob.glob(dir + '/*'):
    # make hardlinks to point to needed pickles
    cmd = 'ln -s {} c_gene_orf_pos_map_pickles'.format(pfile)
    # tried to make hardlink, but 'operation not permitted' on rcstor
    subprocess.call(cmd, shell=True)
