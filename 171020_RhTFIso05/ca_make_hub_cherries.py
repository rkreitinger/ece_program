# from cherry tables, make cherry files to go from src_collection to hub plts

with open('ca_cherry_RhTFIso05_db_hubs.txt', 'w') as ofile:
    for line in open('ab_db_hub_pos.tsv').readlines()[1:]:
        fds = line.strip().split('\t')
        orf, splt, spos, dplt, dpos = fds
        ofile.write('RA001,1,{},{},IRAT,20,{},{}\r\n'.format(splt, spos, dplt, dpos))


with open('ca_cherry_RhTFIso05_ad_hubs.txt', 'w') as ofile:
    for line in open('ab_ad_hub_pos.tsv').readlines()[1:]:
        fds = line.strip().split('\t')
        orf, splt, spos, dplt, dpos = fds
        ofile.write('RA001,1,{},{},IRAT,20,{},{}\r\n'.format(splt, spos, dplt, dpos))
