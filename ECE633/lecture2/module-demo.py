# -*- coding: utf-8 -*-
"""
Created on Thu Aug 24 04:21:30 2017
Source=byte-of-python
@author: Marios Pattichis
"""
# Import a module.
#  Within the module access entries using:
#     <moduleName>.<Name>
# To avoid the need for this, use

#
 

helpString = """
from <moduleName> import <functionName>
     allows you to use <functionName> directly
"""
print(helpString)
from math import sqrt
print("sqrt({})={}".format(4,sqrt(4)))


helpString = """
dir()
   gives you all currently available functions.
"""
print(helpString)
print("Current module functions:\n", dir())

# tfDemo.py
import tensorflow as tf
hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello))

# opencvDemo.py
import numpy as np
import cv2

# Load an color image in grayscale
img = cv2.imread('flower.jpg',0)
cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()

helpString = """
dir(<moduleName>)
 returns list of names from that specified module.
"""
print(helpString)
print("cv2 functions\n", dir(cv2))
