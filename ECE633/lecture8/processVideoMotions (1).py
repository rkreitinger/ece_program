# -*- coding: utf-8 -*-
"""
   The code process the video and produces dense video motions plots.
   There is failure in the method and this shown as -inf in the magnitude plot.
Demo taken from:
   http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html
"""

import cv2
import numpy as np

# Function from: http://programmingcomputervision.com/
def draw_flow(im,flow,step=16):
    """ Plot optical flow at sample points
        spaced step pixels apart. """
        
    h,w = im.shape[:2]
    y,x = np.mgrid[step/2:h:step,step/2:w:step].reshape(2,-1)
    fx,fy = flow[y.astype(int),x.astype(int)].T
        
    # create line endpoints
    lines = np.vstack([x,y,x+fx,y+fy]).T.reshape(-1,2,2)
    lines = np.int32(lines)
    
    # create image and draw
    vis = cv2.cvtColor(im,cv2.COLOR_GRAY2BGR)
    for (x1,y1),(x2,y2) in lines:
        cv2.line(vis,(x1,y1),(x2,y2),(0,255,0),1)
        cv2.circle(vis,(x1,y1),1,(0,255,0), -1)
    
    return vis



cap = cv2.VideoCapture('vtest.avi')

ret, frame1 = cap.read()
prvs = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
hsv = np.zeros_like(frame1)
hsv[...,1] = 255

print("Press Esc on the image to exit")
print("Press s   on the image to save")
delay_display = 30  # in milliseconds

while(cap.isOpened()):
    success_flag, color_frame = cap.read()
    if (not success_flag):        
        break
        
    current_frame = cv2.cvtColor(color_frame, cv2.COLOR_BGR2GRAY)    
    flow = cv2.calcOpticalFlowFarneback(prvs, current_frame, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    prvs = current_frame
    
    mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)

    print ('Motion magnitude min='+str(mag.min())+' max='+str(mag.max()))
    
    motion_vectors = draw_flow(current_frame, flow)
    cv2.imshow('Motion vector plot', motion_vectors)
    
    vis = np.concatenate((color_frame, bgr), axis=1)
    cv2.imshow('Motion magnitude plot', vis)
        
    k = cv2.waitKey(delay_display) & 0xff
    if k == 27:
        break
    elif k == ord('s'):
        cv2.imwrite('opticalfb.png',frame2)
        cv2.imwrite('opticalhsv.png',bgr)    

cap.release()
cv2.destroyAllWindows()