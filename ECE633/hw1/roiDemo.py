# -*- coding: utf-8 -*-
"""
Created on Thu Aug 31 09:09:08 2017

@author: Marios Pattichis
https://github.com/opencv/opencv_contrib/blob/master/modules/tracking/samples/tracker.py
"""
import numpy as np
import cv2
import sys

# Read image
im = cv2.imread("flower.jpg")

# Select ROI
r = cv2.selectROI("ROI", im)
print("Selected ROI = ", r)

# Crop image
imCrop = im[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
cv2.imshow("selROI", imCrop)

print("Press any key on the image to exit")   
cv2.waitKey(0)
cv2.destroyAllWindows()

