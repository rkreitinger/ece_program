# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 17:48:17 2017

@author: Marios Pattichis
"""

# Example 1
# variable scope demo

# Variables defined outside function 
# has a global scope:
x = 50


def changeGlobal(new_x):
    global x           # x refers to global variable
    local_var = new_x  # none visible outside function
    x = local_var

print('Global x is ', x)
changeGlobal(1)
print('Global x changed to ', x)

# Example 2
# default parameters and naming inputs

# Function definition with default values
def func(a, b=5, c=10):
    print('a is', a, 'and b is', b, 'and c is', c)

print("Function arguments")
print("1. Use default values if needed")
print("2. Assign named variables regardless of definition order.")
print("3. If no names are given, assign from left to right.")
func(1)
func(3, 7)
func(25, c=24)
func(c=50, a=100)


# Note that default arguments must be at placed
# at the end. The following will cause an error:
# def func2(a=6, b, c=8):
#     print('a is', a, 'and b is', b, 'and c is', c)
    