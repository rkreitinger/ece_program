# -*- coding: utf-8 -*-
"""
Simple file to demonstrate the basics of Python.
source file = "byte-of-python.pdf"
@author: Marios Pattichis
"""

# A comment starting with #

# Basic Strings:
StringVar1 = "A string with double quotes"
StringVar2 = 'A string with single quotes' 

LongString = """ 
A long multi-line string 
This is line 2 "with double quotes inside"
This is line 3 'with single quotes inside'
"""
             
# Printing strings:
print(StringVar1)
print(StringVar2)
print(LongString)

# Basic concatenation:
ConcatEx = "A = " + str(5) + "\n"
print(ConcatEx)    

# Print using multiple , also works:
print('Print multiple numbers using commas: num1 = ', 2, ' num2 = ', 3)    
    
# Formatted Printing:
# - Use {} for multiple variables.
# - Use {0:.3f} to specify each string.
# - Use .format to pass the variable to print
# - Use str() to convert and + to concatenate strings
a = 1.0/3
b = 1/9.0
print('{}, {},'.format(a,b)+str(3/2))
print('{0:.3f}'.format(a) + ' and {0:.5f}'.format(b))

# All variables are objects

# Indentation:
# - No indentation for top level.
# - Same number of spaces for each level
# - Must maintain the same number of spaces for empty lines also.


# Number guessing game
# --------------------
number = 10
guess  = int(input('Guess my integer number : '))

# Nested if using if, elif, else
if (guess == number):
    # Indent with the same number of spaces for this block
    print('Congratulations, you guessed it.')
elif (guess < number):
    print('No, it is higher than that')
else:
    print('No, it is lower')

# ---------------------

# for, in, range
print('range(len) gives 0, 1, ..., len-1')
len = 5
for i in range(len):
    print(i)

startIndex = 1
print('range(startIndex, len) gives start_index, ..., len-1')
for i in range(startIndex, len):
    print(i)

# while, break, continue
while True:
  s = input('Enter q to quit : ')
  if (s == 'q'):
      break    # get out of the loop
  else:
      print('Skip the rest of the loop with continue')
      continue # skip this part
  print('This print statement is never executed')
