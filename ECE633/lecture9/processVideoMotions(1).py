# -*- coding: utf-8 -*-
"""
   The code process the video and produces dense video motions plots.
   There is failure in the method and this shown as -inf in the magnitude plot.
Demo taken from:
   http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html
"""

import cv2
import numpy as np
import math  as math

# draw_flow():
#  im:   the grayscale image to draw on.
#  flow: the (u, v) motion vectors.
#  step: the number of pixels to skip between vectors. 
# Function from: http://programmingcomputervision.com/
def draw_flow(im,flow,step=8):
    """ Plot optical flow at sample points
        spaced step pixels apart. """
    h,w = im.shape[:2]
    y,x = np.mgrid[step/2:h:step,step/2:w:step].reshape(2,-1)
    fx,fy = flow[y.astype(int),x.astype(int)].T
        
    # create line endpoints
    lines = np.vstack([x,y,x+fx,y+fy]).T.reshape(-1,2,2)
    lines = np.int32(lines)
    
    # create image and draw
    vis = cv2.cvtColor(im,cv2.COLOR_GRAY2BGR)
    for (x1,y1),(x2,y2) in lines:
        mag  = np.sqrt((x1-x2)**2 + (y1-y2)**2)
        ang  = math.degrees(math.atan2(y1-y2, x1-x2))
        
        # Use 1.75 for Eating1Round1Side.mp4
        cond = (mag > 1.5) and (mag < 6) # and ((ang > -45) or (ang < 45))
        if (cond):
            cv2.line(vis,(x1,y1),(x2,y2),(0,255,0),1)
            cv2.circle(vis,(x1,y1),1,(0,255,0), -1)
    
    return vis



framesToSkip = 5;
reduceFactor = 0.5;
print("Keeping one out of ", framesToSkip, " video frames")
print("Images reduced by ", reduceFactor)

#cap = cv2.VideoCapture('videos/SideCameraClips/Typing1Round1Side.mp4')
cap = cv2.VideoCapture('videos/BackCameraClips/Writing2Round2.mp4')

ret, frame1_full = cap.read()
frame1 = cv2.resize(frame1_full, (0,0), fx=reduceFactor, fy=reduceFactor) 

prvs = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
hsv = np.zeros_like(frame1)
hsv[...,1] = 255


firstTime = False
print("Press Esc on the image to exit")
print("Press s   on the image to save")
delay_display = 1  # in milliseconds

while(cap.isOpened()):
    for i in range(framesToSkip):
        success_flag, full_frame = cap.read()
        if (not success_flag):
            break
    
    if (not success_flag):
        break
    
    color_frame = cv2.resize(full_frame, (0,0), fx=reduceFactor, fy=reduceFactor) 
        
    current_frame = cv2.cvtColor(color_frame, cv2.COLOR_BGR2GRAY)    
    
    flow = cv2.calcOpticalFlowFarneback(prvs, current_frame, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    # calculate optical flow
    # p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)

    
    prvs = current_frame
    
    mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)

    #print ('Motion magnitude min='+str(mag.min())+' max='+str(mag.max()))
    
    motion_vectors = draw_flow(current_frame, flow)
    cv2.imshow('Motion vector plot', motion_vectors)
    
    vis = np.concatenate((color_frame, bgr), axis=1)
    cv2.imshow('Motion', vis)
    
    # Process key pressed:
    #  Esc will escape, s will save the image.
    k = cv2.waitKey(delay_display) & 0xff
    if k == 27:
        break
    elif k == ord('s'):
        cv2.imwrite('opticalfb.png',frame2)
        cv2.imwrite('opticalhsv.png',bgr)    
    
    if (firstTime):
        res = input("Please center the display and hit any key")
        firstTime = False

# Release the video
cap.release()

# Close all the OpenCV Windows
cv2.destroyAllWindows()