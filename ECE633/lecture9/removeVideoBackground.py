# -*- coding: utf-8 -*-
"""
  Simple bakcground subtraction demo. Only shows the example that works in Python.
References:
  http://docs.opencv.org/master/db/d5c/tutorial_py_bg_subtraction.html
"""

import numpy as np
import cv2

cap  = cv2.VideoCapture('videos/SideCameraClips/Typing1Round1Side.mp4')
fgbg = cv2.createBackgroundSubtractorMOG2()

print("Press ESC to stop bakcground subtraction")
while(cap.isOpened()):
    ok, frame=cap.read()
    if not ok:
        break
    
    fgmask = fgbg.apply(frame)
    
    cv2.imshow('MOG2 Esc to exit',fgmask)        
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()