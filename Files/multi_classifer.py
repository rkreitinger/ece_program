"""
@author: Rebecca Kreitinger

"""

from __future__ import (division, print_function, unicode_literals)

import numpy as np
import os

import matplotlib
import matplotlib.pyplot as plt

plt.rcParams['axes.labelsize'] = 14
plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12

PROJECT_ROOT_DIR = "."
CHAPTER_ID = "classification"


def save_fig(fig_id, tight_layout=True):
	path = os.path.join(PROJECT_ROOT_DIR, "images", CHAPTER_ID, fig_id + ".png")
	print("Saving figure", fig_id)
	if tight_layout: 
		plt.tight_layout()
	plt.savefig(path, format='png', dpi=300)


from sklearn.datasets import fetch_mldata
mnist = fetch_mldata('MNIST original')
#mnist


cells = {'COL_NAMES': ['label', 'data'],
	'DESCR': 'cells-first',
	'data':np.array([])}


import os
import glob
import cv2
import numpy as np
from numpy import array

images = []
count = 0
for file in glob.glob('../171020_RhTFIso05/fa_extract_colony_images/Rh*/*jpg'):
	fname = os.path.basename(file)
	cell = cv2.imread(file, 0)
	data = cell.reshape((1, -1))
	
#	image_data = data[0].tolist()
	images.append(data[0])
#	if (count == 0):
#		all_data = np.array([data[0]])
#		print(all_data)
#	else:
#		image = np.array([data[0]])
#		image_data = np.concatenate((all_data,image), axis=0)
#		all_data = image_data
				
#	count += 1

scores = []	
for file in glob.glob('../171020_RhTFIso05/eb_iscore_results/Rh*/*tsv'):
	fname = os.path.basename(file)

	file = open(file, 'r')
	lines = file.readlines()[0:96]
	file.close()
	
	for line in lines:
		col = line.split()
		if (len(col)>=1):
			scores.append(col[1])
			#np.append(cells['target'], col[1])
			#cells.setdefault('target',[]).append(col[1])
	

score_data = np.array(scores)
#image_data = np.array(all_data)
print(score_data)
print (score_data.size)

#file = open("test.txt", "w")
#file.write(image_data.array_str())
#file.close()
#print (image_data) 

#cells.setdefault('data',[]).append(image_data, axis = 0)

#print(cells)

import math as math

#x, y = mnist["data"], mnist["target"]
#print("All images: x shape = ", x.shape, ", y shape = ", y.shape)
#print("First image : x[0].shape = ", x[0].size, "=", math.sqrt(x[0].size), "x", math.sqrt(x[0].size))
#print("First image classification: y[0] = ", y[0])
#print("First image: x[0] = \n", x[0], "\n")


#some_digit = x[36000]
#some_digit_image = some_digit.reshape(28, 28)
#plt.imshow(some_digit_image, cmap = matplotlib.cm.binary, interpolation = "nearest")
#plt.axis("off")

#save_fig("some_digit_plot")
#plt.show()

def plot_digit(data):
	image = data.reshape(28,28)
	plt.imshow(image, cmap = matplotlib.com.binary, interpolation = "nearest") 
	plt.axis("off")


#print("Demo: Arranging images into a sequence of arrays:")
#instances = np.array([[1,2,3,4], [5,6,7,8]])
#images = [instance.reshape(2,2) for instance in instances]
#print(images)
	
