# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 11:20:58 2018

@author: Rebecca
"""
# To support both python 2 and python 3
from __future__ import division, print_function, unicode_literals

# Common imports
import numpy as np
import os

# to make this notebook's output stable across runs
np.random.seed(42)

# To plot pretty figures

import matplotlib
import matplotlib.pyplot as plt
plt.rcParams['axes.labelsize'] = 14
plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12

# Where to save the figures
PROJECT_ROOT_DIR = "."
CHAPTER_ID = "classification"

# Notes: You must have created the directory:
#    ./images/classification
# where all of the results will be stored.
def save_fig(fig_id, tight_layout=True):
    path = os.path.join(PROJECT_ROOT_DIR, "images", CHAPTER_ID, fig_id + ".png")
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format='png', dpi=300)
    
    # Notes: Requires internet connection to fetch the dataset.
#        Need to wait a little bit for the data to arrive.
from sklearn.datasets import fetch_mldata
mnist = fetch_mldata('MNIST original')
mnist

# Notes: 
#   X: Each row is an input, binary digit image.
#   y: Each entry is the digit classification.
import math as math
X, y = mnist["data"], mnist["target"]
print("All images:   X shape =", X.shape, ", y shape = ", y.shape)
print("First image: X[0].shape = ", X[0].size, 
      "=", math.sqrt(X[0].size), "x", math.sqrt(X[0].size))
print("First image classification: y[0] = ", y[0])
print("First image:   X[0] = \n", X[0], "\n")



some_digit = X[36000]
some_digit_image = some_digit.reshape(28, 28)
plt.imshow(some_digit_image, cmap = matplotlib.cm.binary,
           interpolation="nearest")
plt.axis("off")

save_fig("some_digit_plot")
plt.show()

def plot_digit(data):
    image = data.reshape(28, 28)
    plt.imshow(image, cmap = matplotlib.cm.binary,
               interpolation="nearest")
    plt.axis("off")
    
    # Notes:
#  Nice function for plotting 10 (default) images per row!
#  To modify it for any image, start from size=28 ...
#  **options is an optional dictionary for imshow()
#  np.concatenate(row_images, axis=1) adds an image in the same row.
#  np.concatenate(row_images, axis=0) add a full row of images.

# Notes:
#  A short demo showing how images are arranged
#  array by array:

print("Demo: Arranging images into a sequence of arrays:")
import numpy as np

instances = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
images = [instance.reshape(2,2) for instance in instances]
print(images)


# EXTRA
def plot_digits(instances, images_per_row=10, **options):
    size = 28
    images_per_row = min(len(instances), images_per_row)
    images = [instance.reshape(size,size) for instance in instances]
    n_rows = (len(instances) - 1) // images_per_row + 1
    row_images = []
    n_empty = n_rows * images_per_row - len(instances)
    images.append(np.zeros((size, size * n_empty)))
    for row in range(n_rows):
        rimages = images[row * images_per_row : (row + 1) * images_per_row]
        row_images.append(np.concatenate(rimages, axis=1))
    image = np.concatenate(row_images, axis=0)
    plt.imshow(image, cmap = matplotlib.cm.binary, **options)
    plt.axis("off")
    
    # Note: Create 9x9 inches
plt.figure(figsize=(9,9))

# Note: Row-wise merge:
example_images = np.r_[X[:12000:600], X[13000:30600:600], X[30600:60000:590]]
print("Shape of example_images = ", example_images.shape)

plot_digits(example_images, images_per_row=10)
save_fig("more_digits_plot")
plt.show()

y[36000]

X_train, X_test, y_train, y_test = X[:60000], X[60000:], y[:60000], y[60000:]
print("Train size = ", y_train.size)
print("Test  size = ", y_test.size)
# Two approaches for classifying using multiple classes:
#  1. OvO: One versus one. 
#      Only choice for SVM. Everyone competes against everyone else.
#      The one with most of the points wins.
#  2. OvA: One versus all.
#      Binary classifier of one versus the rest.
#      The best one wins. This is the default for binary classification.

# Run the full set with all of the possible digit classifications.
# Runs the default OvA.
sgd_clf.fit(X_train, y_train)
sgd_clf.predict([some_digit])

# Combines multiple individual classifiers into one result
# based on the best score.
some_digit_scores = sgd_clf.decision_function([some_digit])
some_digit_scores

# Max score gives the classifier.
np.argmax(some_digit_scores)

# The labels for each class:
sgd_clf.classes_

# And the label for this class:
sgd_clf.classes_[5]

# Change the paradigm using OvO
from sklearn.multiclass import OneVsOneClassifier
ovo_clf = OneVsOneClassifier(SGDClassifier(random_state=42))
ovo_clf.fit(X_train, y_train)
ovo_clf.predict([some_digit])

# We have  10*9/2 = 45 classifiers.
len(ovo_clf.estimators_)

# Random forest directly provide multi-label classification.
forest_clf.fit(X_train, y_train)
forest_clf.predict([some_digit])

# Excellent probability classification for all of the digits.
forest_clf.predict_proba([some_digit])

# Classification accuracy for each one of the three subsets.
cross_val_score(sgd_clf, X_train, y_train, cv=3, scoring="accuracy")

# SOS: You should have pre-processed the images!!!
# Here, apply X = (x-mean)/std
#  to generate a zero-mean and std=1 image.
# Results are dramatically improved.
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train.astype(np.float64))
cross_val_score(sgd_clf, X_train_scaled, y_train, cv=3, scoring="accuracy")

# Create a nice confusion matrix of everything versus everything else:
y_train_pred = cross_val_predict(sgd_clf, X_train_scaled, y_train, cv=3)
conf_mx = confusion_matrix(y_train, y_train_pred)
conf_mx