from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

#Imports
import numpy as np
import tensorflow as tf
import os
import glob
import cv2

tf.logging.set_verbosity(tf.logging.INFO)


def cnn_model_fn(features, labels, mode):
	
	input_layer = tf.reshape(features["x"], [-1, 28, 28, 1])

	#Con Layer 1
	conv1 = tf.layers.conv2d(
		inputs=input_layer,
		filters=32, 
		kernel_size=[5,5],
		padding="same",
		activation=tf.nn.relu)

	#Pool Layer 1
	pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2,2], strides=2)

	#con Layer 2 and Pool Layer 2
	conv2 = tf.layers.conv2d(
		inputs=pool1, 
		filters=64,
		kernel_size=[5,5],
		padding="same",
		activation=tf.nn.relu)
	pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2,2], strides=2)


	#Dense Layer
	pool2_flat = tf.reshape(pool2, [-1, 7 * 7 * 64])
	dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=tf.nn.relu)
	dropout = tf.layers.dropout(
		inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)

	#Logits Layer
	logits = tf.layers.dense(inputs=dropout, units=6)

	predictions = {
		"classes": tf.argmax(input=logits, axis=1),
		"probabilities": tf.nn.softmax(logits, name="softmax_tensor")
	}

	if mode == tf.estimator.ModeKeys.PREDICT:
		return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

	loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

	if mode == tf.estimator.ModeKeys.TRAIN:
		optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
		train_op = optimizer.minimize(
			loss=loss,
			global_step=tf.train.get_global_step())
		return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
	
	eval_metric_ops = {
		"accuracy":  tf.metrics.accuracy(
			labels=labels, predictions=predictions["classes"])}

	return tf.estimator.EstimatorSpec(
		mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def getTrainImages ():
	
	images = []
	for file in glob.glob('../171020_RhTFIso05/fa_extract_colony_images/Rh*/*jpg'):
		fname = os.path.basename(file)
		cell = cv2.imread(file, 0)
		resized = cv2.resize(cell, (28,28), interpolation = cv2.INTER_AREA)
		data = resized.reshape((1, -1))
		images.append(data[0])
	images = np.asarray(images, dtype=np.float32)
	return images


def getTrainData ():
	
	scores = []
	for file in glob.glob('../171020_RhTFIso05/eb_iscore_results/Rh*/*tsv'):
		fname = os.path.basename(file)
		file = open(file, 'r')
		lines = file.readlines()[0:96]
		file.close()

		for line in lines:
			col = line.split()
			if (len(col)>=1):
				if (col[1] == 'NA'):
					value = '5'
				else:
					value = col[1]
				scores.append(value)
	
	score_data = np.asarray(scores, dtype=np.int32)
	return score_data

def getTestImages():
	test_images = []
	for file in glob.glob('../171020_RhTFIso05/fb_extract_colony_images_test/Rh*/*jpg'):
		fname = os.path.basename(file)
		cell = cv2.imread(file, 0)
		resized = cv2.resize(cell, (28,28), interpolation = cv2.INTER_AREA)
		data = resized.reshape((1,-1))
		test_images.append(data[0])
	test_images = np.asarray(test_images, dtype=np.float32)
	return test_images


def getTestData():
	test_scores = []
	for file in glob.glob('../171020_RhTFIso05/eb_iscore_results/Test/Rh*/*tsv'):
		fname = os.path.basename(file)
		file = open(file, 'r')
		lines = file.readlines()[0:96]
		file.close()
		
		for line in lines:
			col = line.split()
			if (len(col)>=1):
				if(col[1] == 'NA'):
					value = '5'
				else:
					value = col[1]
				test_scores.append(value)
	test_data = np.asarray(test_scores, dtype=np.int32)
	return test_data


def main(unused_argv):
	train_data = getTrainImages()
	train_labels = getTrainData()
	eval_data = getTestImages()
	eval_labels = getTestData()

	cell_classifier = tf.estimator.Estimator(
		model_fn=cnn_model_fn, model_dir="/tmp/cell_convnet_model")

	tensors_to_log = {"probabilities": "softmax_tensor"}
	logging_hook = tf.train.LoggingTensorHook(
		tensors=tensors_to_log, every_n_iter=50)


	train_input_fn = tf.estimator.inputs.numpy_input_fn(
		x={"x": train_data},
		y=train_labels,
		batch_size=100,
		num_epochs=None,
		shuffle=True)
	cell_classifier.train(
		input_fn=train_input_fn,
		steps=20000,
		hooks=[logging_hook])

	eval_input_fn = tf.estimator.inputs.numpy_input_fn(
		x={"x": eval_data}, 
		y = eval_labels, 
		num_epochs=1,
		shuffle=False)
	eval_results = cell_classifier.evaluate(input_fn=eval_input_fn)
	print(eval_results)


if __name__ == "__main__":
	tf.app.run()
